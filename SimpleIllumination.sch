EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D1
U 1 1 5F887C3E
P 4400 2900
F 0 "D1" H 4393 2645 50  0000 C CNN
F 1 "LED" H 4393 2736 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 4400 2900 50  0001 C CNN
F 3 "~" H 4400 2900 50  0001 C CNN
	1    4400 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5F889236
P 3800 2900
F 0 "R1" H 3870 2946 50  0000 L CNN
F 1 "150R" H 3870 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3730 2900 50  0001 C CNN
F 3 "~" H 3800 2900 50  0001 C CNN
	1    3800 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 2900 4250 2900
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5F892E99
P 3450 2900
F 0 "J1" H 3368 3117 50  0000 C CNN
F 1 "Conn_01x02" H 3368 3026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Horizontal" H 3450 2900 50  0001 C CNN
F 3 "~" H 3450 2900 50  0001 C CNN
	1    3450 2900
	-1   0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5F893E92
P 5150 2800
F 0 "H1" H 5250 2846 50  0000 L CNN
F 1 "MountingHole" H 5250 2755 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 5150 2800 50  0001 C CNN
F 3 "~" H 5150 2800 50  0001 C CNN
	1    5150 2800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F8942D1
P 5150 3100
F 0 "H2" H 5250 3146 50  0000 L CNN
F 1 "MountingHole" H 5250 3055 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 5150 3100 50  0001 C CNN
F 3 "~" H 5150 3100 50  0001 C CNN
	1    5150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2900 4550 2900
Wire Wire Line
	4750 2900 4750 3250
Wire Wire Line
	4750 3250 3650 3250
Wire Wire Line
	3650 3250 3650 3000
$Comp
L power:GND #PWR?
U 1 1 5F8EED82
P 3650 3250
F 0 "#PWR?" H 3650 3000 50  0001 C CNN
F 1 "GND" H 3655 3077 50  0000 C CNN
F 2 "" H 3650 3250 50  0001 C CNN
F 3 "" H 3650 3250 50  0001 C CNN
	1    3650 3250
	1    0    0    -1  
$EndComp
Connection ~ 3650 3250
$EndSCHEMATC
